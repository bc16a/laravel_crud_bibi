<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Empresa;
use App\Salario;

class Colaborador extends Model
{
    //
    protected $fillable = ['nome', 'idade','empresa_id'];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function salario()
    {
        return $this->hasOne(Salario::class);
    }

    protected $table = 'colaboradores';
}
