<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Grupo;
use App\Colaborador;

class Empresa extends Model
{
    //
    protected $fillable = ['nome', 'endereco'];

    public function grupos()
    {
        return $this->belongsToMany(Grupo::class);
    }

    public function colaboradores()
    {
        return $this->hasMany(Colaborador::class);
    }
}
