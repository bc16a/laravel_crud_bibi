<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Validator;

class AuthController extends Controller
{
    //

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Erros de validação']);
        }
        // tenta fazer login com os dados recebidos, se der certo
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = auth()->user();
            // se o usuario nao tiver um token valido cria um token novo
            if ($user->token() == '') {
                $token = $user->createToken('web')->accessToken;
            }
            // se ja tiver token valido usa ele
            else {
                $token = $user->token();
            }
            // retorna o usuario e o token
            return response()->json(['usuario'=>$user, 'token' => $token ,'mensagem'=> 'sucesso']);
        }
        // se nao conseguir fazer login com as credenciais recebidas
        return response()->json([ 'mensagem'=> 'Credenciais inválidas.',null, 401]);

    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function registro(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Erros de validação']);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $usuario = User::create($input);
        $dadosResposta['token'] = $usuario->createToken('web')->accessToken;
        $dadosResposta['user'] = $usuario;
        return response()->json(['usuario'=>$dadosResposta,'mensagem'=> 'Usuário registrado com sucesso.', 201]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        
        return response()->json([ 'mensagem'=> 'Usuário fez Logout.',null, 200]);
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function meuPerfil(Request $request)
    {
        if ($request->user() == null) {
            return response()->json([ 'mensagem'=> 'Usuário não encontrado.',null, 400]);

        }
        return response()->json(['usuario'=>$request->user(), 'mensagem'=> 'Usuário encontrado.',null, 200]);
    }
}
