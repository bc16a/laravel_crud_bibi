<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;

class ClienteController extends Controller
{
    //
     //
   public function  index ()
   {
       $resposta=['dados'=> User::all(), 
       'mensagem' => 'Sucesso'];
       return response()->json($resposta);
   }

   public function show (Request $request)
   {   
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
        

       $user=User::find($request->id);
       if ($user)
       {
       $resposta=['dados'=> $user, 'mensagem' => 'Sucesso'];
       return response()->json($resposta);
       }else 
       return response()->json(['mensagem'=> 'Não exite esse usuario']);
   }
   
   public function store (Request $request)
   {  
      
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|max:255',
        ]);

       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
        
        $user = User::where('email', $request->email)->first();
        if ($user) {
            return response()->json([ 'mensagem'=> 'Email já pertence à outro cliente.']);
        }

          // Cria um usuário
          $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);
        $dadosResposta['token'] = $user->createToken('web')->accessToken;
        $dadosResposta['user'] = $user;
        
        $user->password = bcrypt($user->password);
        $user->save();

       return response()->json(['user'=>$user, 'mensagem'=> 'sucesso']);
   }

   public function update (Request $request)
   {  
    $validator = Validator::make($request->all(), [
        'id' => 'required|integer',
        'name' => 'required|max:255',
        'email' => 'required|max:255',
        'password' => 'required|max:255'
    ]);

    
       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
       
        $user = User::where('email', $request->email)->first();
        if ($user && ($user->id != $request->id)) {
            return response()->json([ 'mensagem'=> 'Email já pertence à outro cliente.']);
        }

       $user=User::find($request->id);

      // Atualiza as informações
      $user->name = $request->name;
      $user->email = $request->email;
      $user->password = $request->password;
      $dadosResposta['token'] = $user->createToken('web')->accessToken;
      $dadosResposta['user'] = $user;

      $user->password = bcrypt($user->password);
      $user->save();

       return response()->json(['grupo'=>$user, 'mensagem'=> 'sucesso']);
   }



   public function  destroy(Request $request)
   {  
       $validator = Validator::make($request->all(),
       ['id' => 'required|integer',
      ]);

     
       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Id incorreto']);

        $user = User::find($request->id);
        if ($user->admin)
        {
            return response()->json(['mensagem'=> 'Não é possivél excluir o usuario.']);
        }

       if ($user)
       {
           $user->delete();
           return response()->json(['mensagem'=> 'Apagar com sucesso']);
       }else return response()->json(['mensagem'=> 'Id não existe']);
   }

}
