<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Empresa;
use App\Grupo;
use App\Colaborador;

class ColaboradorController extends Controller
{
    //
    public function  index (Request $request)
    {
 
        $validator = Validator::make($request->all(),
        [
            'empresa_id'=> 'required|integer'
        ]);
    
        if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
        
        $empresa=Empresa::find($request->empresa_id);
    
        if (!$empresa)
        {
            return response()->json(['mensagem'=> 'Não exite']);
        }
    
        if($empresa->colaboradores->count() > 0)
        {
            return response()->json($empresa->colaboradores);
        }
    
            $resposta=['dados'=> Colaborador::all(), 
            'mensagem' => 'Sucesso'];
            return response()->json($resposta);
    }
 
    public function show (Request $request)
    {   
 
        $colaborador=Colaborador::find($request->id);
        if ($colaborador)
        {
        $resposta=['dados'=> $colaborador, 
        'mensagem' => 'Sucesso'];
        return response()->json($resposta);
        }else 
        return response()->json(['mensagem'=> 'Não exite esse usuario']);
    }
    
    public function store (Request $request)
    {  
        $validator = Validator::make($request->all(),
        ['empresa_id'=> 'required|integer', 
         'nome'=> 'required|max:255', 
         'idade'=> 'required|max:255'
        ]);
 
        if ($validator->fails())
         return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
         
        $empresa = Empresa::find($request->empresa_id);
        if(!$empresa){
         return response()->json(['mensagem'=> 'Id não existe']);
        }
 
        $colaborador = new Colaborador;
        $colaborador->nome =$request->nome;
        $colaborador->idade =$request->idade;

        $empresa = $empresa->colaboradores()->save($colaborador);
 
        return response()->json(['empresa'=>$empresa, 'mensagem'=> 'sucesso']);
    }
 
    public function update (Request $request)
    {  
       
        $validator = Validator::make($request->all(),
        ['empresa_id'=> 'required|integer', 
         'nome'=> 'required|max:255', 
         'idade'=> 'required|max:255',
         'colaborador_id'=> 'required|integer',
        ]);
 
        if ($validator->fails())
         return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);

        $colaborador = Colaborador::find($request->colaborador_id);

        $empresa = Empresa::find($request->empresa_id);
        if(!$empresa){
         return response()->json(['mensagem'=> 'Id não existe']);
        }
 
        $colaborador->empresa()->associate($empresa);
        $colaborador->nome= $request->nome;
        $colaborador->idade=$request->idade;
        $colaborador->save();
 
        return response()->json(['colaborador'=>$colaborador, 'mensagem'=> 'sucesso']);
    }
 
 
 
    public function  destroy(Request $request)
    {  
        $validator = Validator::make($request->all(),
        ['id' => 'required|integer',
       ]);
 
      
        if ($validator->fails())
         return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Id incorreto']);
 
        $empresa=Colaborador::find($request->id);
 
        if ($empresa)
        {
            $empresa->delete();
            return response()->json(['mensagem'=> 'Apagar com sucesso']);
        }else return response()->json(['mensagem'=> 'Id não existe']);
    }
 
    // public function removeGrupo(Request $request)
    // {  
 
 
    //  $validator = Validator::make($request->all(),
    //  [
    //  'grupo_id'=> 'required|integer',
    //  'empresa_id'=> 'required|integer',
    //  ]);
 
    //  if ($validator->fails())
    //   return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
 
 
    //  $grupo = Grupo::find($request->grupo_id);
    //  $empresa = Empresa::find($request->empresa_id);
 
    //  if(!$grupo || !$empresa){
    //   return response()->json(['mensagem'=> 'Id não existe']);
    //  }
 
    //  $empresa->grupos()->detach($grupo);
 
    //  return response()->json(['empresa'=>$empresa, 'mensagem'=> 'Grupo não esta mais relacionado']);
   
    // }
}
