<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Empresa;
use App\Grupo;

class EmpresaController extends Controller
{
    //
    //
   public function  index (Request $request)
   {

    $validator = Validator::make($request->all(),
    ['grupo_id'=> 'required|integer'
    ]);

    if ($validator->fails())
    return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
    
    $grupo=Grupo::find($request->grupo_id);

    if (!$grupo)
    {
        return response()->json(['mensagem'=> 'Não exite']);
    }

    if($grupo->empresas->count() > 0)
    {
        return response()->json($grupo->empresas);
    }

       $resposta=['dados'=> Empresa::all(), 
       'mensagem' => 'Sucesso'];
       return response()->json($resposta);
   }

   public function show (Request $request)
   {   

       $empresa=Empresa::find($request->id);
       if ($empresa)
       {
       $resposta=['dados'=> $empresa, 
       'mensagem' => 'Sucesso'];
       return response()->json($resposta);
       }else 
       return response()->json(['mensagem'=> 'Não exite esse usuario']);
   }
   
   public function store (Request $request)
   {  
       $validator = Validator::make($request->all(),
       ['grupo_id'=> 'required|integer', 
        'nome'=> 'required|max:255', 
        'endereco'=> 'required|max:255'
       ]);

       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
        
       $empresa = Empresa::create([
           'grupo_id'=>$request->grupo_id,
           'nome'=>$request->nome,
           'endereco'=>$request->endereco
       ]);


       $grupo = Grupo::find($request->grupo_id);
       if(!$grupo){
        return response()->json(['mensagem'=> 'Id não existe']);
       }

       $empresa->save();
       $empresa->grupos()->attach($grupo);

       return response()->json(['empresa'=>$empresa, 'mensagem'=> 'sucesso']);
   }

   public function update (Request $request)
   {  
       $validator = Validator::make($request->all(),
       [
        'id' => 'required|integer',
        'nome'=> 'required|max:255',
        'endereco'=> 'required|max:255' 
      ]);

     
       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
       
       $empresa=Empresa::find($request->id);

       $empresa->update([
        'nome'=>$request->nome,
        'endereco'=>$request->endereco
      ]);

       return response()->json(['empresa'=>$empresa, 'mensagem'=> 'sucesso']);
   }



   public function  destroy(Request $request)
   {  
       $validator = Validator::make($request->all(),
       ['id' => 'required|integer',
      ]);

     
       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Id incorreto']);

       $empresa=Empresa::find($request->id);

       if ($empresa)
       {
           $empresa->delete();
           return response()->json(['mensagem'=> 'Apagar com sucesso']);
       }else return response()->json(['mensagem'=> 'Id não existe']);
   }

   public function removeGrupo(Request $request)
   {  


    $validator = Validator::make($request->all(),
    [
    'grupo_id'=> 'required|integer',
    'empresa_id'=> 'required|integer',
    ]);

    if ($validator->fails())
     return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);


    $grupo = Grupo::find($request->grupo_id);
    $empresa = Empresa::find($request->empresa_id);

    if(!$grupo || !$empresa){
     return response()->json(['mensagem'=> 'Id não existe']);
    }

    $empresa->grupos()->detach($grupo);

    return response()->json(['empresa'=>$empresa, 'mensagem'=> 'Grupo não esta mais relacionado']);
  
   }

}
