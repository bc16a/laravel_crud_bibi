<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Grupo;
use App\User;

class GrupoController extends Controller
{
   //
   public function  index ()
   {
       $resposta=['dados'=> Grupo::all(), 
       'mensagem' => 'Sucesso'];
       return response()->json($resposta);
   }

   public function show (Request $request)
   {   

       $grupo=Grupo::find($request->id);
       if ($grupo)
       {
       $resposta=['dados'=> $grupo, 
       'mensagem' => 'Sucesso'];
       return response()->json($resposta);
       }else 
       return response()->json(['mensagem'=> 'Não exite esse usuario']);
   }
   
   public function store (Request $request)
   {  
       $validator = Validator::make($request->all(),
       ['nome'=> 'required|max:255','user_id' => 'required|integer']);

       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Cliente não encontrado.']);
        }
        
       
    //    $grupo = Grupo::create([
    //        'nome'=>$request->nome
    //    ]);

       $grupo = new Grupo;
       $grupo->nome =$request->nome;

       $user = $user->grupo()->save($grupo);

       return response()->json(['user'=>$grupo, 'mensagem'=> 'sucesso']);
   }

   public function update (Request $request)
   {  
       $validator = Validator::make($request->all(),
       [
        'id' => 'required|integer',
        'nome'=> 'required|max:255', 
        'user_id' => 'required|integer',
      ]);

     
       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
       
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Cliente não encontrado.']);
        }

       $grupo=Grupo::find($request->id);

       $grupo->update(['nome'=>$request->nome
      ]);

      $grupo->user()->associate($user);

       return response()->json(['grupo'=>$grupo, 'mensagem'=> 'sucesso']);
   }



   public function  destroy(Request $request)
   {  
       $validator = Validator::make($request->all(),
       ['id' => 'required|integer',
      ]);

     
       if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Id incorreto']);

       $grupo=Grupo::find($request->id);

       if ($grupo)
       {
           $grupo->delete();
           return response()->json(['mensagem'=> 'Apagar com sucesso']);
       }else return response()->json(['mensagem'=> 'Id não existe']);
   }


}
