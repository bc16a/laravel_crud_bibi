<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Empresa;
use App\Grupo;
use App\Colaborador;
use App\Salario;

class SalarioController extends Controller
{
    //
    public function  index (Request $request)
    {
 
        $validator = Validator::make($request->all(),
        [
            'colaborador_id'=> 'required|integer'
        ]);
    
        if ($validator->fails())
        return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
        
        $colaborador=Colaborador::find($request->colaborador_id);
    
        if (!$colaborador)
        {
            return response()->json(['mensagem'=> 'Não exite']);
        }
    
        if($colaborador->salario->count() > 0)
        {
            return response()->json($colaborador->salario);
        }
    
            $resposta=['dados'=> Salario::all(), 
            'mensagem' => 'Sucesso'];
            return response()->json($resposta);
    }
 
    public function show (Request $request)
    {   
 
        $salario=Salario::find($request->id);
        if ($salario)
        {
        $resposta=['dados'=> $salario, 
        'mensagem' => 'Sucesso'];
        return response()->json($resposta);
        }else 
        return response()->json(['mensagem'=> 'Não exite esse usuario']);
    }
    
    public function store (Request $request)
    {  
        $validator = Validator::make($request->all(),
        ['colaborador_id'=> 'required|integer', 
        'valor'=> 'required|integer'
        ]);
 
        if ($validator->fails())
         return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
         
        $colaborador = Colaborador::find($request->colaborador_id);
        if(!$colaborador){
         return response()->json(['mensagem'=> 'Id não existe']);
        }
 
        $salario = new Salario;
        $salario->valor =$request->valor;

        $colaborador = $colaborador->salario()->save($salario);
 
        return response()->json(['salario'=>$salario, 'mensagem'=> 'sucesso']);
    }
 
    public function update (Request $request)
    {  
    
        $validator = Validator::make($request->all(),
        ['colaborador_id'=> 'required|integer', 
        'valor'=> 'required|integer',
        'salario_id'=> 'required|integer',
        ]);
 
        if ($validator->fails())
         return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);

        $salario = Salario::find($request->salario_id);

        $colaborador = Colaborador::find($request->colaborador_id);
        if(!$colaborador){
         return response()->json(['mensagem'=> 'Id não existe']);
        }
 
        $salario->colaborador()->associate($colaborador);
        $salario->valor= $request->valor;
        $salario->save();
 
        return response()->json(['salario'=>$salario, 'mensagem'=> 'sucesso']);
    }
 
 
 
    public function  destroy(Request $request)
    {  
        $validator = Validator::make($request->all(),
        ['id' => 'required|integer',
       ]);
 
      
        if ($validator->fails())
         return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Id incorreto']);
 
        $salario=Salario::find($request->id);
 
        if ($salario)
        {
            $salario->delete();
            return response()->json(['mensagem'=> 'Apagar com sucesso']);
        }else return response()->json(['mensagem'=> 'Id não existe']);
    }
 
    // public function removeGrupo(Request $request)
    // {  
 
 
    //  $validator = Validator::make($request->all(),
    //  [
    //  'grupo_id'=> 'required|integer',
    //  'empresa_id'=> 'required|integer',
    //  ]);
 
    //  if ($validator->fails())
    //   return response()->json(['error'=>$validator->errors(), 'mensagem'=> 'Falha']);
 
 
    //  $grupo = Grupo::find($request->grupo_id);
    //  $empresa = Empresa::find($request->empresa_id);
 
    //  if(!$grupo || !$empresa){
    //   return response()->json(['mensagem'=> 'Id não existe']);
    //  }
 
    //  $empresa->grupos()->detach($grupo);
 
    //  return response()->json(['empresa'=>$empresa, 'mensagem'=> 'Grupo não esta mais relacionado']);
   
    // }
}
