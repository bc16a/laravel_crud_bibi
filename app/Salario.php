<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Colaborador;

class Salario extends Model
{
    //
    protected $fillable = ['valor', 'colaborador_id'];

    public function colaborador()
    {
        return $this->belongsTo(Colaborador::class);
    }
}
