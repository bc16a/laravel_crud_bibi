<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@registro');

Route::middleware(['auth:api'])->group(function(){

    Route::post('createGrupo', 'GrupoController@store');
    Route::get('mostraTodo', 'GrupoController@index');
    Route::post('mostraUm', 'GrupoController@show');
    Route::post('atualizaUM', 'GrupoController@update');
    Route::post('Apagar', 'GrupoController@destroy');

    Route::post('createEmpresa', 'EmpresaController@store');
    Route::post('mostraTodoEmpresa', 'EmpresaController@index');
    Route::post('mostraUmaEmpresa', 'EmpresaController@show');
    Route::post('atualizaUMEmpresa', 'EmpresaController@update');
    Route::post('ApagarEmpresa', 'EmpresaController@destroy');

    Route::post('createColaborador', 'ColaboradorController@store');
    Route::post('mostraTodoColaborador', 'ColaboradorController@index');
    Route::post('mostraUmaColaborador', 'ColaboradorController@show');
    Route::post('atualizaUMColaborador', 'ColaboradorController@update');
    Route::post('ApagarColaborador', 'ColaboradorController@destroy');

    Route::post('createSalario', 'SalarioController@store');
    Route::post('mostraTodoSalario', 'SalarioController@index');
    Route::post('mostraUmaSalario', 'SalarioController@show');
    Route::post('atualizaUMSalario', 'SalarioController@update');
    Route::post('ApagarSalario', 'SalarioController@destroy');

    Route::post('createUser', 'ClienteController@store');
    Route::post('atualizaUser', 'ClienteController@update');
    Route::post('deletaUser', 'ClienteController@destroy');
    Route::get('userTodo', 'ClienteController@index');
    Route::post('userUm', 'ClienteController@show');

    Route::get('logout', 'AuthController@logout');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
